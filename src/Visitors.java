import java.util.HashMap;
import java.util.Map;

public class Visitors implements VistorInterface{
    public int count_leaf = 0;
    public int count_exp = -1;
    public Map<String, Integer> symb_table = new HashMap<String, Integer>();
    StringBuilder output = new StringBuilder();
    public int visit_leaf(Stm s) {
        return 0;
    }
    public int visit_leaf(){
        count_leaf++;
        return 0;
    }

    @Override
    public int visit_leaf(CompoundStm c) {
        return 0;
    }


    public void visit_symbol(String sym, int val) {
        symb_table.put(sym, val);
    }

    @Override
    public void visit_print() {

    }

    public void visit_print(IdExp tmp, String id) {
        output.append(id);
    }

    public void visit_print(AssignStm tmp) {
        output.append(" := ");
    }
}
