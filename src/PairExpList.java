public class PairExpList extends ExpList {
    public Exp head;
    public ExpList tail;
    public PairExpList(Exp h, ExpList t) {
        head = h;
        tail = t;
    }

    public int accepts(Visitors visit){
        try {
//            visit.output.append("(");
            head.accepts(visit);
            visit.output.append(", ");
            tail.accepts(visit);
        }
        catch (Exception e){
            return 0;
        }

        return 0;
    }
}
