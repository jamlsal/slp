public class LastExpList extends ExpList {
    public Exp head;
    public LastExpList(Exp h) {
        head = h;
    }
    public int accepts(Visitors visit){
        try {
            int tmp = head.accepts(visit);
            visit.output.append(")");
            return tmp;
        }
        catch (Exception e){
            return 0;
        }
    }
}
