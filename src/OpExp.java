public class OpExp extends Exp {
    public Exp left, right;
    public int oper;
    final public static int
    Plus = 1,
    Minus = 2,
    Times = 3,
    Div = 4;
    public OpExp(Exp l, int o, Exp r) {
        left = l;
        oper = o;
        right = r;

    }

    public int accepts(Visitors visit){
        try {
            int val = 0;
            int val_l = left.accepts(visit);
            switch (oper){
                case 1: visit.output.append(" + "); break;
                case 2: visit.output.append(" - "); break;
                case 3: visit.output.append(" * "); break;
                case 4: visit.output.append(" / "); break;
                default: break;
            }
            int val_r = right.accepts(visit);

            switch (oper){
                case 1: val = val_l + val_r; break;
                case 2: val = val_l - val_r; break;
                case 3: val = val_l * val_r; break;
                case 4: val = val_l / val_r; break;
                default: break;
            }

            return val;
        }
        catch (Exception e){
            return 0;
        }
    }
}
