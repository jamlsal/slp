public class AssignStm extends Stm {
    public IdExp id;
    public Exp exp;
    public AssignStm(IdExp i, Exp e) {
        id = i;
        exp = e;
    }

    public int accepts(Visitors visit){
        try {
            id.accepts(visit);
            visit.output.append(" := ");
            int val = exp.accepts(visit);
            visit.visit_symbol(id.id, val);
        }
        catch (Exception e){
            return 0;
        }
        return 0;
    }
}
