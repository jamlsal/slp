public  class PrintStm extends Stm {
    public ExpList exps;
    public PrintStm(ExpList e) {
        exps = e;
    }

    public int accepts(Visitors visit){
        try {
            visit.output.append("print(");
            exps.accepts(visit);
        }
        catch (Exception e){
            return 0;
        }

        return 0;
    }
}
