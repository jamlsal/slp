public class IdExp extends Exp{
    public String id;
    public IdExp(String i) {
        id = i;
    }

    public int accepts(Visitors visit){
        try {
            visit.visit_leaf();
            visit.output.append(id);
            return visit.symb_table.get(id);
        }
        catch (Exception e){
            return 0;
        }
    }
}
