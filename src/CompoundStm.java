public class CompoundStm extends Stm {
    public Stm stm1, stm2;

    public CompoundStm(Stm s1, Stm s2) {
        stm1 = s1;
        stm2 = s2;
    }

    public int accepts(Visitors visit) {
        try {
            stm1.accepts(visit);
            visit.output.append(";\n");
            stm2.accepts(visit);
//            visit.output = place + ";\n" + place2;
        } catch (Exception e) {
            return 0;
        }

        return 0;
    }
}