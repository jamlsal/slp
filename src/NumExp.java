public class NumExp extends Exp {
    public int num;
    public NumExp(int n ){
        num = n;
    }

    public int accepts(Visitors visit){
        try {
            visit.visit_leaf();
            visit.output.append(num);
            return num;

        }
        catch (Exception e){
            return 0;
        }
    }
}
