public class EseqExp extends Exp {
    public Stm stm;
    public Exp exp;
    public EseqExp(Stm s, Exp e) {
        stm = s;
        exp = e;
    }

    public int accepts(Visitors visit){
        try {
            visit.count_exp++;
            visit.output.append("(\n\t").append("\t".repeat(visit.count_exp));
            stm.accepts(visit);
            visit.output.append(",\n\t").append("\t".repeat(visit.count_exp));
            int tmp =  exp.accepts(visit);
//            visit.count_exp--;
            visit.output.append("\n").append("\t".repeat(visit.count_exp)).append(")");
            visit.count_exp--;
            return tmp;
        }
        catch (Exception e){
            return 0;
        }
    }
}
