import javax.xml.xpath.XPathEvaluationResult;

// Main.java
// James Salazar
// COSC4503 Program Translation
// SLP Visitors
// 2/8/24
// Satus: DONE
//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Stm prog = new CompoundStm(
                new AssignStm(
                        new IdExp("a"),
                        new OpExp(
                                new NumExp(5), OpExp.Plus, new NumExp(3))),
                new CompoundStm(
                        new AssignStm(
                                new IdExp("b"),
                                new EseqExp(
                                        new PrintStm(new PairExpList(
                                                new IdExp("a"),
                                                new LastExpList(new OpExp(new IdExp("a"), OpExp.Minus, new NumExp(1)))
                                        )),
                                        new OpExp(new NumExp(10), OpExp.Times, new IdExp("a"))
                                )
                                ), new PrintStm((new LastExpList(new IdExp("b"))))
                )
        );

        Visitors  visit = new Visitors();
        int test = prog.accepts(visit);

        System.out.println("Number of leaf nodes: " + visit.count_leaf);
        System.out.println("The Symbol Table: ");
        System.out.println("Symbol   |   Value");
        for (String i : visit.symb_table.keySet()) {
            System.out.println("  " + i + "      |    " + visit.symb_table.get(i));
        }
        System.out.println("Pretty Print:");
        System.out.println(visit.output.toString());

//        a := 3;
//        b := a * 4;
//        c := (print(b), b+a);
//        d := (e := 5, (print((print(e), 6), (f := e / f, e-f)), 8))

        Stm prog2 = new CompoundStm(
                // a := 3;
                new AssignStm( new IdExp("a"), new NumExp(3)),
                new CompoundStm(
                //      b := a * 4;
                        new AssignStm(new IdExp("b"), new OpExp(new IdExp("a"), OpExp.Times, new NumExp(4))),
                                new CompoundStm(
                                        // c := (print(b), b+a);
                                        new AssignStm(new IdExp("c"), new EseqExp(new PrintStm(new LastExpList(new IdExp("b"))),
                                                new OpExp(new IdExp("b"), OpExp.Plus, new IdExp("a")))),
//                                      d := (e := 5, (print((print(e), 6), (f := e / f, e-f)), 8))
                                        new AssignStm(
                                                new IdExp("d"),
                                                new EseqExp(
                                                        new AssignStm(new IdExp("e"), new NumExp(5)),
                                                        new EseqExp(
                                                                new PrintStm(new PairExpList(
                                                                        new EseqExp(
                                                                                new PrintStm(new LastExpList(new IdExp("e"))),
                                                                                new NumExp(6)
                                                                        ),
                                                                        new LastExpList(new EseqExp(
                                                                                new AssignStm(new IdExp("f"), new OpExp(new IdExp("e"), OpExp.Div, new NumExp(7))),
                                                                                new OpExp(new IdExp("e"), OpExp.Minus, new IdExp("f"))
                                                                        )))),
                                                                new NumExp(8)
                                                        )
                                                )))));
        Visitors visit2 = new Visitors();
        int test2 = prog2.accepts(visit2);


        System.out.println("\n-----------------------------------\n");

        System.out.println("Number of leaf nodes: " + visit2.count_leaf);
        System.out.println("The Symbol Table: ");
        System.out.println("Symbol   |   Value");
        for (String i : visit2.symb_table.keySet()) {
            System.out.println("  " + i + "      |    " + visit2.symb_table.get(i));
        }
        System.out.println("Pretty Print:");
        System.out.println(visit2.output.toString());


        System.out.println("DONE");

    }
}