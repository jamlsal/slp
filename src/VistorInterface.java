public interface VistorInterface {
    int visit_leaf(CompoundStm c);
    int visit_leaf(Stm s);
    int visit_leaf();
    void visit_symbol(String sym, int val);
    void visit_print();
}
